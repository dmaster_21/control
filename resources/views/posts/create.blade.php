@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Detalle del Post</div>
                <form action="{{action('PostsController@store')}}" method="POST">
                <div class="card-body">
      
		       <input type="hidden" name="_token"value="{{ csrf_token()}}">         

                <hr>
                <label>DETALLE:</label>
                <textarea class='form-control' name='texto'></textarea>

               <hr>
                <button type='submit' class='btn btn-success'>Guardar </button>
               
                
                   </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection