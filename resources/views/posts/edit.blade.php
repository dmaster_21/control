@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

            @if (session('mensaje'))
                        <div class="alert alert-success" role="alert">
                            {{ session('mensaje') }}
                        </div>
                    @endif
                <div class="card-header">Detalle del Post</div>
                <form action="{{action('PostsController@update',$consulta->id)}}" method="POST">
                <div class="card-body">
               
           <input type="hidden" name="_method" value="PUT">
		<input type="hidden" name="_token"value="{{ csrf_token()}}">
                <label>ID:</label>
                <input class='form-control' name='id' value="{{$consulta->id}}" readonly>

                <hr>
                <label>DETALLE:</label>
                <textarea class='form-control' name='texto'>{{$consulta->texto}}</textarea>

               <hr>
                <button type='submit' class='btn btn-success'>Guardar Detalle</button>
               
                
                   </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection