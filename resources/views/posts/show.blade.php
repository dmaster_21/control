@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Detalle del Post</div>

                <div class="card-body">
                
                <label>ID:</label>
                {{$consulta->id}}

                <hr>
                <label>DETALLE:</label>
                {{$consulta->texto}}
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection