
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">AQUI ESTAN TUS COMENTARIOS </div>
                @if (session('mensaje'))
                        <div class="alert alert-success" role="alert">
                            {{ session('mensaje') }}
                        </div>
                    @endif
                <div class="card-body">
                <table class='table table-hover'>
                <thead>
                <tr>
                <td>Id</td>
                <td>Comentario</td>
                <td>Action</td>

                </tr>
                </thead>
                <tbody>
                @foreach($consulta as $post)
                <tr>
                <td>{{ $post->id}}</td>
                <td>{{ $post->texto}}</td>
                <td>
                <a href="{{url('posts/'.$post->id)}}"><button class='btn btn-primary btn-sm'>Ver</button></a>
                <a href="{{url('posts/'.$post->id.'/edit')}}"><button class='btn btn-warning btn-sm'>Editar</button></a>
                <form action="{{action('PostsController@destroy',$post->id)}}" method='POST'>
                <input type="hidden" name="_method" value="DELETE">
		<input type="hidden" name="_token"value="{{ csrf_token()}}">
                <button type='submit' class='btn btn-danger btn-sm'>Eliminar</button>
                </form>


                
                </td>

                </tr>


                @endforeach


                </tbody>
                
                </table>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection