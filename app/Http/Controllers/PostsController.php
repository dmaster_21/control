<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Post;
class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     $consulta=Post::all();
      return view('posts.index',compact('consulta'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $posts= New Post();
        $posts->texto=$request->texto;
        $posts->save();
        Session::flash('mensaje', 'El Post Fue Creado de Manera Satisfactoria.');
        Session::flash('class', 'success');
        return redirect('posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $consulta=Post::find($id);
        return view('posts.show',compact('consulta'));
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $consulta=Post::find($id);
       
        return view('posts.edit',compact('consulta'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $posts= Post::find($id);
       $posts->texto=$request->texto;
       $posts->save();
       Session::flash('mensaje', 'El Post Fue Editado de Manera Satisfactoria.');
       Session::flash('class', 'success');

       return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $posts= Post::find($id);
        $posts->delete();
        Session::flash('mensaje', 'El Post Fue eliminado  de Manera Satisfactoria.');
        Session::flash('class', 'success');
 
        return redirect('posts');
    }
}
